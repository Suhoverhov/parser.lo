<?php
set_time_limit(0);
require_once('lib/phpQuery.php');

// Подключаем класс для работы с excel
require_once('lib/phpExcel/PHPExcel.php');

$phpexcel = new PHPExcel();
$page = $phpexcel->setActiveSheetIndex(0);

$domain = "http://www.ultima-fishing.com";
$target = "http://www.ultima-fishing.com/pricelist/";
$pattern = '/\.(.*)\./';
preg_match($pattern, $domain, $matches);
$title = $matches[1];
$dir = 'images/'.$title;
if ( !is_dir($dir) ) {
	mkdir($dir);
}

	$ultima = curlStart($target);

if ( !empty($ultima) ) {
		$document = phpQuery::newDocumentHTML($ultima);
		$pq = pq($document);

		// получение и сохранение элементов каталога
		$catalogList = $pq->find(".cpt_maincontent table:last-child tr:not('.background1')");

		$counter = 1;
		// проход по каталогу
		foreach($catalogList as $itemList) {
			$itemList = pq($itemList)->find("td:nth-child(2) a")->attr('href');
			$currentCatalog = $domain.$itemList;

			$catalogData = curlStart($currentCatalog);
			$document = phpQuery::newDocumentHTML($catalogData);
			$pq = pq($document);

			$tovarPage = $pq->find('.cpt_maincontent form table:last-child');
			$tovarname = trim(pq($tovarPage)->find('div.cpt_product_name h1')->text());
			$tovarImg = trim(pq($tovarPage)->find('#prddeatailed_container .cpt_product_images > div > div a')->attr("href"));
			if (empty($tovarImg)) {
				$tovarImg = trim(pq($tovarPage)->find('#prddeatailed_container .cpt_product_images > div > div img')->attr("src"));
			}
			$tovarImg = $domain.$tovarImg;
			if ($tovarImg == $domain) {
				$tovarImg = '';
			}
			$temp = explode('/', $tovarImg);
			$imgName = end($temp);

			$content = file_get_contents($tovarImg);
			$path = $dir.'/'.$imgName;
			if ($content) {
				if ( !file_exists($path) ) {
					file_put_contents($path, $content);
				}
			}
			else {
				echo "Файл ".$tovarImg." не существует. Название товара ".$tovarname."</br>";
				$imgName = "нет изображения";
			}

			$page->setCellValue("A".$counter, $tovarname);
			$page->setCellValue("B".$counter, $imgName);
			$counter++;
		}
}
$objWriter = PHPExcel_IOFactory::createWriter($phpexcel, 'Excel2007');
$objWriter->save($title.".xlsx");

/**
 * Получить инфу со страницы
 * @param $adr
 * @return bool|mixed
 */
function curlStart($adr) {
	if ($curl = curl_init()) {
		curl_setopt($curl, CURLOPT_URL, $adr);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.57 Safari/537.36");
		curl_setopt($curl, CURLOPT_AUTOREFERER, true);
		$kaida = curl_exec($curl);
		curl_close($curl);
		return $kaida;
	}

	return false;
}
