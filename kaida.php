<?php

require_once('lib/phpQuery.php');

// Подключаем класс для работы с excel
require_once('lib/phpExcel/PHPExcel.php');

$phpexcel = new PHPExcel();
$page = $phpexcel->setActiveSheetIndex(0);

$domain = "http://www.kaida-fishing.ru/";
$pattern = '/\.(.*)\./';
preg_match($pattern, $domain, $matches);
$title = $matches[1];
$dir = 'images/'.$title;
if ( !is_dir($dir) ) {
	mkdir($dir);
}

	$kaida = curlStart($domain);

	if ( !empty($kaida) ) {
		$document = phpQuery::newDocumentHTML($kaida);
		$pq = pq($document);

		// получение и сохранение элементов каталога
		$catalogList = $pq->find('.dtree_cont table td.dtreefirst a');

		// каталог товаров
		$hrefList = array();
		foreach($catalogList as $itemList) {
			$hrefList[] = pq($itemList)->attr('href');
		}

		$counter = 1;
		// проход по каталогу
		foreach($hrefList as $itemList) {
			$currentCatalog = $domain.$itemList."&show_all=yes";

			$catalogData = curlStart($currentCatalog);
			$document = phpQuery::newDocumentHTML($catalogData);
			$pq = pq($document);

			$tovarUrls = $pq->find('.center_block2 div.tovar2 p a');
//			$fff = 0;
			foreach($tovarUrls as $itemTovarUrl) {

//				if ( $fff == 2 ) break;
//				$fff++;

				$itemTovarUrl = pq($itemTovarUrl)->attr('href');
				$itemTovarUrl = $domain.$itemTovarUrl;

				$tovar = curlStart($itemTovarUrl);
				$documentTovar = phpQuery::newDocumentHTML($tovar);
				$pqTovar = pq($documentTovar);
				$tovarNameObj = $pqTovar->find("div.center_block2");
				$tovarName = pq($tovarNameObj)->find('h1')->text();

				$img = pq($tovarNameObj)->find('div.tovar  div.tovar_img img')->attr("id");
				if ( empty($img) ) {
					$img = pq($tovarNameObj)->find('div.tovar  div.tovar_img img')->attr("src");
					$images = explode('/', $img);
					$img = end($images);
				}

				$imgname = $img;
				$img = $domain."data/big/".$img;

				$content = file_get_contents($img);
				if (!$content) {
					$img = str_replace('big', 'medium', $img);
					$content = file_get_contents($img);
				}

				$path = $dir.'/'.$imgname;
				if ($content) {
					if ( !file_exists($path) ) {
						file_put_contents($path, $content);
					}
				}
				else {
					echo "Файл ".$img." не существует. Название товара ".$tovarName;
					$imgname = "нет изображения";
				}

				$otherTovarsNames = pq($tovarNameObj)->find('table tr');

				if($otherTovarsNames->length > 0 ) {

					$iter = 0;
					foreach($otherTovarsNames as $otherTovarNameItem) {
						if ($iter != 0) {

							$otherTovarNameItemTd = pq($otherTovarNameItem)->find('td');

							$tekCt = 0;
							$currentTovName = '';
							$currentSize = '';
							foreach($otherTovarNameItemTd as $otherTovarNameItem2) {
								if ($tekCt == 0) {
									$currentTovName = trim(pq($otherTovarNameItem2)->text());
								}

								if ($tekCt == 1) {
									$currentSize = " ( Размер: ".trim(pq($otherTovarNameItem2)->text())."; )";
									break;
								}
								$tekCt++;
							}
							$currentTovName .= $currentSize;
							$page->setCellValue("A".$counter, $currentTovName);
							$page->setCellValue("B".$counter, $imgname);
							$counter++;
						}
						$iter++;
					}
				}
				else {
					$page->setCellValue("A".$counter, trim($tovarName));
					$page->setCellValue("B".$counter, $imgname);
					$counter++;
				}
			}
		}
	}
$objWriter = PHPExcel_IOFactory::createWriter($phpexcel, 'Excel2007');
$objWriter->save($title.".xlsx");

/**
 * Получить инфу со страницы
 * @param $adr
 * @return bool|mixed
 */
function curlStart($adr) {
	if ($curl = curl_init()) {
		curl_setopt($curl, CURLOPT_URL, $adr);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.57 Safari/537.36");
		curl_setopt($curl, CURLOPT_AUTOREFERER, true);
		$kaida = curl_exec($curl);
		curl_close($curl);
		return $kaida;
	}

	return false;
}
