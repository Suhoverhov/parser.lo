<?php

require_once('lib/phpQuery.php');

// Подключаем класс для работы с excel
require_once('lib/phpExcel/PHPExcel.php');

$phpexcel = new PHPExcel();
$page = $phpexcel->setActiveSheetIndex(0);

$domain = "http://www.kit003.ru";
$target = "http://www.kit003.ru/index.php?option=com_k2&view=itemlist&layout=category&task=category&id=1&Itemid=3";
$pattern = '/\.([^\/]*)\./';
preg_match($pattern, $domain, $matches);
$title = $matches[1];
$dir = 'images/'.$title;
if ( !is_dir($dir) ) {
	mkdir($dir);
}

$kit003 = curlStart($target);

	if ( !empty($kit003) ) {
		$document = phpQuery::newDocumentHTML($kit003);
		$pq = pq($document);
//
		// получение и сохранение элементов каталога
		$catalogList = $pq->find('#k2Container .itemListSubCategories .subCategoryContainer table h2 a');
//
		// каталог товаров
		$hrefList = array();
		foreach($catalogList as $itemList) {
			$hrefList[] = pq($itemList)->attr('href');
		}

		$counter = 1;
		// проход по каталогу
		foreach($hrefList as $itemList) {
			$currentCatalog = $domain.$itemList;

//			if( $currentCatalog == "http://www.kit003.ru/index.php?option=com_k2&view=itemlist&task=category&id=3:summer&Itemid=8") {/////////////////////
				$catalogData = curlStart($currentCatalog);
				parseToCats($catalogData);
	//			parseToList($subCat);
//			}

		}
	}
$objWriter = PHPExcel_IOFactory::createWriter($phpexcel, 'Excel2007');
$objWriter->save($title.".xlsx");

/**
 * Получить инфу со страницы
 * @param $adr
 * @return bool|mixed
 */
function curlStart($adr) {
	if ($curl = curl_init()) {
		curl_setopt($curl, CURLOPT_URL, $adr);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.57 Safari/537.36");
		curl_setopt($curl, CURLOPT_AUTOREFERER, true);
		$kaida = curl_exec($curl);
		curl_close($curl);
		return $kaida;
	}

	return false;
}

/**
 * Проход по страницам товара
 * @param $subCatUrlObj
 */
function parseToList($subCat) {
	global $domain;
	global $dir;
	global $page;
	global $counter;
	parseToCats($subCat);
	$documentSubCat = phpQuery::newDocumentHTML($subCat);
	$pqSubCat = pq($documentSubCat);
	$subCatUrlObj = $pqSubCat->find("#itemListLeading .itemContainer .catItemHeader h3 a");
	if ($subCatUrlObj->length > 0) {
		foreach($subCatUrlObj as $subCatUrlItem) {
			$subCatUrlItem = pq($subCatUrlItem)->attr('href');
			$subCatUrlItem = $domain.$subCatUrlItem;


			$tovPage = curlStart($subCatUrlItem);
			$documentTov = phpQuery::newDocumentHTML($tovPage);
			$pqTov = pq($documentTov);

			$tovarNameObj = $pqTov->find("#k2Container");
			$tovarName = pq($tovarNameObj)->find(".itemHeader h2")->text();

//			if ( empty($tovarName) ) {
//				$tovarName = pq($tovarNameObj)->find(".itemList .itemContainer .catItemHeader h3.catItemTitle a")->text();
//			}

			$img = pq($tovarNameObj)->find('.itemBody .itemImageBlock .itemImage a')->attr("href");

//			if ( empty($tovarName) ) {
//				$img = pq($tovarNameObj)->find('.itemList .itemContainer .catItemView .catItemBody .catItemImageBlock .catItemImage ')->attr("href");
//			}
			//				if ( empty($img) ) {
			//					$img = pq($tovarNameObj)->find('div.tovar  div.tovar_img img')->attr("src");
			//					$images = explode('/', $img);
			//					$img = end($images);
			//				}

			$arrayNames = explode('/', $img);
			$imgname = end($arrayNames);
			//				$img = $domain."data/big/".$img;
			//
			$content = file_get_contents($img);
			//				if (!$content) {
			//					$img = str_replace('big', 'medium', $img);
			//					$content = file_get_contents($img);
			//				}
			//
			$path = $dir.'/'.$imgname;
			if ($content) {
				if ( !file_exists($path) ) {
					file_put_contents($path, $content);
				}
			}
			else {
				echo "Файл ".$img." не существует. Название товара ".$tovarName."</br>";
				$imgname = "нет изображения";
			}

			$page->setCellValue("A".$counter, trim($tovarName));
			$page->setCellValue("B".$counter, $imgname);
			$counter++;
		}
	}
}

function parseToCats($catalogData) {
	global $domain;
	$document = phpQuery::newDocumentHTML($catalogData);
	$pq = pq($document);

	$subCategoryUrls = $pq->find('.itemListSubCategories .subCategoryContainer .subCategory table h2 a');
//			$fff = 0;
	if ( $subCategoryUrls->length > 0 ) {
		foreach($subCategoryUrls as $itemSubCategoryUrl) {

	//				if ( $fff == 2 ) break;
	//				$fff++;

			$itemSubCategoryUrl = pq($itemSubCategoryUrl)->attr('href');
			$itemSubCategoryUrl = $domain.$itemSubCategoryUrl;

//			if( $itemSubCategoryUrl == "http://www.kit003.ru/index.php?option=com_k2&view=itemlist&task=category&id=12:aksessuary_leto&Itemid=8") {/////////////////////
				$subCat = curlStart($itemSubCategoryUrl);
				parseToList($subCat);
//			}/////////////////////////////////////////

		}
	}

//	$checkCategs = $pq->find('.itemListCategoriesBlock .itemListSubCategories .subCategoryContainer');
//	if () {
////		parseToCats($catalogData);
//	}

}